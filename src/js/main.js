(function($) {
	$('.js-menu-open').on('click', function () {
		$('.response_menu').show();
	});
	$('.js-menu-close').on('click', function(){
		
		$('.response_menu').hide();
	});
	$('.js-item-link').on('click', function(e){
		e.preventDefault();

		$(this).find('.dropdown_link').toggleClass('dropdown_link-arrow');
		$(this).find('.dropdown-child-mob').slideToggle();
		
	});
	$('.js-slick-partners').slick({
	dots: true,
    infinite: false,
    slidesToShow: 6,
    focusOnSelect: false,
    prevArrow: '<div><img src="../img/prev.png"></div>',
    nextArrow: '<div><img src="../img/next.png"></div>',
    responsive: [{
      breakpoint: 1000,
      settings: {
        slidesToShow: 4
      }
    },
    {
      breakpoint: 900,
      settings: {
        slidesToShow: 3
      }
    },
     {
      breakpoint: 650,
      settings: {
        slidesToShow: 2
      }
    },
     {
      breakpoint: 380,
      settings: {
      	rows: 3,
        slidesToShow: 1
      }
    }]

	});
	$('.partners_item').css('display', 'flex');
  $('.js-request-btn').on('click', function () {
    $('.js-request-btn').css('background-color', '#00794b');
  });
  $('.js-consultation-btn').on('click', function () {
    $('.js-consultation-btn').css('background-color', '#00794b');
  });
  $('.js-footer-callback').on('click', function (e) {
    e.preventDefault();
    console.log(1);
     $('.js-footer-callback').css('border', '1px solid #02ce81')
    $('.footer-icon-green').css({'display': 'block', 'opacity': '0.5'});
    $('.service_item_callback').css({'opacity': '0.5', 'color': '#02ce81'});
    $('.footer-icon-grey').css({'display': 'none'});
  });

$('.js-gallery-slick').slick({
  dots: true,
    infinite: false,
    rows: 2,
    slidesToShow: 4,
    focusOnSelect: false,
    prevArrow: '<div><img src="../img/prev.png"></div>',
    nextArrow: '<div><img src="../img/next.png"></div>',
     responsive: [{
      breakpoint: 1265,
      settings: {
        slidesToShow: 3
      }
    },
    {
      breakpoint: 800,
      settings: {
        slidesToShow: 2
      }
    },
     {
      breakpoint: 600,
      settings: {
        rows: 1,
        slidesToShow: 1
      }
    }
    ]

  });

$('.js-documentation-slick').slick({
  dots: true,
    infinite: false,
    slidesToShow: 4,
    focusOnSelect: false,
    prevArrow: '<div><img src="../img/prev.png"></div>',
    nextArrow: '<div><img src="../img/next.png"></div>',
     responsive: [{
      breakpoint: 1265,
      settings: {
        slidesToShow: 3
      }
    },
    {
      breakpoint: 900,
      settings: {
        slidesToShow: 2
      }
    },
     {
      breakpoint: 650,
      settings: {
        
        slidesToShow: 1
      }
    }]

  });
$('.js-catalog-dropdown').on('click', function(e){
    e.preventDefault();
    console.log(e.target);
    var target = e.target;
    $(target).toggleClass('arrow-top');
    $(target).next().slideToggle();
});
if($(window).width() < 381){
  $('.js-catalog-child-dropdown').on('click', function(e){
    e.preventDefault();
     var target = e.target;
     $(target).toggleClass('arrow-top');
    $(target).next().slideToggle();
    if($(target).hasClass('arrow-top')){
      $(target).css('color','#00d181');
    }
    else{
      $(target).css('color','inherit');
    }
});
}

$('.js-active-text').on('click', function(){
  $(this).toggleClass('question_open');
  $(this).next().slideToggle();
});

$('.js-pressed-cart').on('click', function() {
  $(this).find('.armature_cart_img').css('display', 'none').find('.focused-cart').css('display', 'none');

});
$('.js-product-gallery').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    vertical: true,
    focusOnSelect: true,
    infinite: false,
    swipeToSlide:true,
     prevArrow: '<div class="rotate-right"><img src="../img/prev.png"></div>',
    nextArrow: '<div class="rotate-left"><img src="../img/next.png"></div>',
    responsive: [{
      breakpoint: 1000,
      settings: {
        vertical: false,
        slidesToShow: 5,
         prevArrow: '<div class="rotate-right"><img src="../img/prev-mob.png"></div>',
    nextArrow: '<div class="rotate-left"><img src="../img/next-mob.png"></div>'
      }
    }]

});
$('.slider-for').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  asNavFor: '.js-product-gallery'
});
})(jQuery);